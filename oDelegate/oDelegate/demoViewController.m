//
//  demoViewController.m
//  oDelegate
//
//  Created by ADMIN on 10/22/13.
//  Copyright (c) 2013 demo. All rights reserved.
//

#import "demoViewController.h"

@interface demoViewController ()

@end

@implementation demoViewController

@synthesize odelegate;

-(void)oProcessCompleted{
    
    NSLog(@"process Completed");
        lb1.text=@"3 seconds completed delegate executed";
    
}

-(void)ostartProcess{
    
    [NSTimer scheduledTimerWithTimeInterval:3.0f target:self.odelegate selector:@selector(oProcessCompleted) userInfo:nil repeats:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    demoViewController *demo=[[demoViewController alloc]init];
    demo.odelegate=self;
    
    [demo ostartProcess];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
