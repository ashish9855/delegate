//
//  demoViewController.h
//  oDelegate
//
//  Created by ADMIN on 10/22/13.
//  Copyright (c) 2013 demo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol oProtocolDelegate <NSObject>

@optional

-(void)oProcessCompleted;

@end

@interface demoViewController : UIViewController<oProtocolDelegate>

{
    id<oProtocolDelegate> odelegate;
    IBOutlet UILabel *lb1;
}

@property(nonatomic,strong)id odelegate;

-(void)ostartProcess;

@end


